import turtle
t = turtle.Pen()
turtle.bgcolor("dark blue")
sides = eval(input("Enter a number of sides between 1 and 11: "))
colors = ["light blue", "yellow", "white", "light pink", "purple", "light green", "green", "brown", "red", "orange", "black", "tan"]
for x in range(360):
    t.pencolor(colors[x%sides])
    t.forward(x * 3/sides + x)
    t.left(360/sides + 1)
    t.left(90)
    t.width(x*sides/200)